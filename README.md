# Automation-Example

Test Assessment for Automation Engineers.

The tools and frameworks used in this project include:

- IntelliJ
- Selenium
- Java 10
- JUnit 5
- Maven
